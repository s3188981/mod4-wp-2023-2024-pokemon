openapi: "3.0.3"
externalDocs:
  url: https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon
info:
  title: PokeApp API
  version: "1.0"
  license:
    name: Creative Commons Attribution 4.0 International (CC-BY-4.0)
    url: https://creativecommons.org/licenses/by/4.0/deed.en
  description: The [PokeApp API](https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon) is a REST API part of a sample web application designed to teach web development concepts at the [Data & Information module](https://www.utwente.nl/en/education/bachelor/programmes/technical-computer-science/study-programme/#modules-technical-computer-science) of the [Technical Computer Science](https://www.utwente.nl/en/education/bachelor/programmes/technical-computer-science/) program of the [University of Twente](https://utwente.nl/en).
  contact:
    name: Claudenir M. Fonseca
    url: https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon/issues
    email: c.moraisfonseca@utwente.nl
servers:
  - description: Local Deployment
    url: http://localhost:8080/pokemon/api
tags:
  - name: Trainer
    description: Routes for managing data about trainers.
  - name: Pokémon
    description: Routes for managing data about pokémon.
  - name: Pokémon Type
    description: Routes for managing data about pokémon types.
paths:
  /trainers:
    get:
      description: Retrieves a collection of resources.
      tags:
        - Trainer
      parameters:
        - name: pageNumber
          description: Returns the desired page.
          in: query
          schema:
            type: number
        - name: pageSize
          description: Returns the desired number of resources.
          in: query
          schema:
            type: number
      responses:
        200:
          description: OK
          content:
            application/json:
              example:
                {
                  "meta": {
                    "total": 1,
                    "pageNumber": 1,
                    "pageSize": 1
                  },
                  "data": [
                    {
                      "id": "1",
                      "name": "Red",
                      "created": "24/04/2024",
                      "lastUpDate": "2024-05-12T23:12:01.660866Z",
                      "profileUrl": "https://archives.bulbagarden.net/media/upload/thumb/d/d3/Lets_Go_Pikachu_Eevee_Red.png/500px-Lets_Go_Pikachu_Eevee_Red.png"
                    }
                  ]
                }

    post:
      description: Creates a resource on the route's collection.
      tags:
        - Trainer
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/Trainer'
            examples:
              full:
                description: Complete POST example
                value: {
                  "name": "Red",
                  "profileUrl": "https://archives.bulbagarden.net/media/upload/thumb/d/d3/Lets_Go_Pikachu_Eevee_Red.png/500px-Lets_Go_Pikachu_Eevee_Red.png"
                }
              min:
                description: Minimum POST example
                value: { }
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "1",
                  "name": "Red",
                  "created": "24/04/2024",
                  "lastUpDate": "2024-05-12T23:12:01.660866Z",
                  "profileUrl": "https://archives.bulbagarden.net/media/upload/thumb/d/d3/Lets_Go_Pikachu_Eevee_Red.png/500px-Lets_Go_Pikachu_Eevee_Red.png"
                }
        400:
          description: Bad request
        405:
          description: Server error
  /trainers/{id}:
    get:
      description: Retrieves a resource with matching a {id}.
      tags:
        - Trainer
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "1",
                  "name": "Red",
                  "created": "24/04/2024",
                  "lastUpDate": "2024-05-12T23:12:01.660866Z",
                  "profileUrl": "https://archives.bulbagarden.net/media/upload/thumb/d/d3/Lets_Go_Pikachu_Eevee_Red.png/500px-Lets_Go_Pikachu_Eevee_Red.png"
                }
        404:
          description: Not found
    put:
      description: Replaces the resource {id} with the provided one.
      tags:
        - Trainer
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/Trainer'
            examples:
              full:
                description: Complete PUT example
                value: {
                  "id": "1",
                  "name": "New Red Name",
                  "profileUrl": null
                }
              min:
                description: Minimum PUT example
                value: { "id": "1" }
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "1",
                  "name": "New Red Name",
                  "created": "24/04/2024",
                  "lastUpDate": "2024-05-12T23:12:01.660866Z",
                  "profileUrl": null
                }
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error
    delete:
      description: Deletes the resource identified as {id}.
      tags:
        - Trainer
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        204:
          description: No content
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error

  /pokemon:
    get:
      description: Retrieves a collection of resources.
      tags:
        - Pokémon
      responses:
        200:
          description: OK
          content:
            application/json:
              example:
                {
                  "meta": {
                    "total": 1,
                    "pageNumber": 1,
                    "pageSize": 1
                  },
                  "data": [
                    {
                      "id": "1",
                      "created": "24/04/2024",
                      "lastUpDate": "2024-05-12T23:12:01.660866Z",
                      "name": "My Pikachu",
                      "pokemonType": "513",
                      "trainerId": "1"
                    }
                  ]
                }
    post:
      description: Creates a resource on the route's collection.
      tags:
        - Pokémon
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/Pokemon'
            examples:
              full:
                value: {
                  "name": "My Pikachu",
                  "pokemonType": "513",
                  "trainerId": "1"
                }
                description: Complete POST example
              min:
                value: { }
                description: Minimum POST example
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "1",
                  "created": "24/04/2024",
                  "lastUpDate": "2024-05-12T23:12:01.660866Z",
                  "name": "My Pikachu",
                  "pokemonType": "513",
                  "trainerId": "1"
                }
        400:
          description: Bad request
        405:
          description: Server error
  /pokemon/{id}:
    get:
      description: Retrieves a resource with matching a {id}.
      tags:
        - Pokémon
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        200:
          description: OK
          content:
            application/json:
              example:
                {
                  "id": "1",
                  "name": "My Pikachu",
                  "created": "24/04/2024",
                  "lastUpDate": "2024-05-12T23:12:01.660866Z",
                  "pokemonType": "513",
                  "trainerId": "1"
                }
        404:
          description: Not found
    put:
      description: Replaces the resource {id} with the provided one.
      tags:
        - Pokémon
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true

      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/Pokemon'
            examples:
              full:
                value: {
                  "id": "1",
                  "name": "It's Blue's charmander",
                  "pokemonType": "4",
                  "trainerId": "2"
                }
                description: Complete PUT example
              min:
                value: {
                  "id": "1"
                }
                description: Minimum PUT example
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "1",
                  "created": "24/04/2024",
                  "lastUpDate": "2024-05-12T23:12:01.660866Z",
                  "name": "It's Blue's charmander",
                  "pokemonType": "4",
                  "trainerId": "2"
                }
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error
    delete:
      description: Deletes the resource identified as {id}.
      tags:
        - Pokémon
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        204:
          description: No content
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error

  /pokemonTypes:
    get:
      description: Retrieves a collection of resources.
      tags:
        - Pokémon Type
      parameters:
        - name: pageNumber
          description: Returns the desired page.
          in: query
          schema:
            type: number
        - name: pageSize
          description: Returns the desired number of resources.
          in: query
          schema:
            type: number
        - name: sortBy
          description: Returns resources sorted according the desired attribute.
          in: query
          schema:
            type: string
            enum:
              - id
              - pokedexNumber
              - lastUpDate
      responses:
        200:
          description: OK
          content:
            'application/json':
              example: {
                "meta": {
                  "total": 1,
                  "pageNumber": 1,
                  "pageSize": 1
                },
                "data": [
                  {
                    "id": "1",
                    "name": "Abomasnow",
                    "created": "24/04/2024",
                    "lastUpDate": "24/04/2024",
                    "pokedexNumber": 460,
                    "generation": 4,
                    "japaneseName": "Yukinoohユキノオー",
                    "classification": "Frosted Tree Pokémon",
                    "abilities": [
                      "Snow Warning",
                      "Soundproof"
                    ],
                    "baseHeight": 2.2,
                    "baseWeight": 135.5,
                    "baseHp": 90,
                    "baseAttack": 132,
                    "baseSpAttack": 132,
                    "baseDefense": 105,
                    "baseSpDefense": 105,
                    "baseSpeed": 30,
                    "captureRate": 60,
                    "isLegendary": false,
                    "imgUrl": "./images/abomasnow.png",
                    "primaryType": "grass",
                    "secondaryType": "ice"
                  }
                ]
              }
    post:
      description: Creates a resource on the route's collection.
      tags:
        - Pokémon Type
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/PokemonType'
            examples:
              partial:
                value: {
                  "id": "804",
                  "name": "New Pokemon Type",
                  "pokedexNumber": 999
                }
                description: Partial POST example
              min:
                value: { }
                description: Minimum POST example
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "804",
                  "name": "New Pokemon Type",
                  "created": null,
                  "lastUpDate": "2024-05-13T08:07:50.212596Z",
                  "pokedexNumber": 999,
                  "generation": 0,
                  "japaneseName": null,
                  "classification": null,
                  "abilities": null,
                  "baseHeight": 0.0,
                  "baseWeight": 0.0,
                  "baseHp": 0,
                  "baseAttack": 0,
                  "baseSpAttack": 0,
                  "baseDefense": 0,
                  "baseSpDefense": 0,
                  "baseSpeed": 0,
                  "captureRate": 0,
                  "isLegendary": false,
                  "imgUrl": null,
                  "primaryType": null,
                  "secondaryType": null
                }
        400:
          description: Bad request
        405:
          description: Server error
  /pokemonTypes/{id}:
    get:
      description: Retrieves a resource with matching a {id}.
      tags:
        - Pokémon Type
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        200:
          description: OK
          content:
            'application/json':
              example:
                {
                  "id": "804",
                  "name": "Modified Pokemon Type",
                  "created": "2024-05-13T08:04:03.186221Z",
                  "lastUpDate": "2024-05-13T08:04:03.186308Z",
                  "pokedexNumber": 1000,
                  "generation": 0,
                  "japaneseName": null,
                  "classification": "The most powerful pokemon",
                  "abilities": null,
                  "baseHeight": 0.0,
                  "baseWeight": 0.0,
                  "baseHp": 0,
                  "baseAttack": 0,
                  "baseSpAttack": 0,
                  "baseDefense": 0,
                  "baseSpDefense": 0,
                  "baseSpeed": 0,
                  "captureRate": 0,
                  "isLegendary": false,
                  "imgUrl": null,
                  "primaryType": null,
                  "secondaryType": null
                }
        404:
          description: Not found
    put:
      description: Replaces the resource {id} with the provided one.
      tags:
        - Pokémon Type
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/PokemonType'
            examples:
              partial:
                value: {
                  "name": "Modified Pokemon Type",
                  "pokedexNumber": 1000
                }
                description: Partial PUT example
              min:
                value: { }
                description: Minimum PUT example
      responses:
        200:
          description: OK
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error
    delete:
      description: Deletes the resource identified as {id}.
      tags:
        - Pokémon Type
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        204:
          description: No content
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error

components:
  schemas:
    Trainer:
      description: Schema describing the overall shape of trainer resources.
      type: object
      properties:
        id:
          type: string
          minLength: 1
        created:
          type: string
          format: date-time
        lastUpdate:
          type: string
          format: date-time
        name:
          type: string
          minLength: 1
        profileUrl:
          type: string
          format: uri
        pokemon:
          type: array
          items:
            type: string
            minLength: 1
        party:
          type: array
          items:
            type: string
            minLength: 1

    Pokemon:
      description: Schema describing the overall shape of pokemon resources.
      type: object
      properties:
        id:
          type: string
          minLength: 1
        created:
          type: string
          format: date-time
        lastUpdate:
          type: string
          format: date-time
        name:
          type: string
          minLength: 1
        pokemonType:
          type: string
          minLength: 1
        trainerId:
          type: string
          minLength: 1

    PokemonType:
      description: Schema describing the overall shape of pokemon type resources.
      type: object
      properties:
        id:
          type: string
          minLength: 1
        created:
          type: string
          format: date-time
        lastUpdate:
          type: string
          format: date-time
        name:
          type: string
          minLength: 1
        pokedexNumber:
          type: number
          minimum: 0
        generation:
          type: number
          minimum: 0
        japaneseName:
          type: string
        classification:
          type: string
        abilities:
          type: array
          items:
            type: string
            minLength: 1
        baseHeight:
          type: number
          minimum: 0
        baseWeight:
          type: number
          minimum: 0
        baseHp:
          type: number
          minimum: 0
        baseAttack:
          type: number
          minimum: 0
        baseSpAttack:
          type: number
          minimum: 0
        baseDefense:
          type: number
          minimum: 0
        baseSpDefense:
          type: number
          minimum: 0
        baseSpeed:
          type: number
          minimum: 0
        captureRate:
          type: number
          minimum: 0
        isLegendary:
          type: boolean
        imgUrl:
          type: string
          format: uri
        primaryType:
          type: string
          enum:
            - bug
            - dark
            - dragon
            - electric
            - fairy
            - fighting
            - fire
            - flying
            - ghost
            - grass
            - ground
            - ice
            - normal
            - poison
            - psychic
            - rock
            - steel
            - water
        secondaryType:
          type: string
          enum:
            - bug
            - dark
            - dragon
            - electric
            - fairy
            - fighting
            - fire
            - flying
            - ghost
            - grass
            - ground
            - ice
            - normal
            - poison
            - psychic
            - rock
            - steel
            - water
